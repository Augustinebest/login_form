import React from 'react';
import Routes from './routes';
import './pages/page.css';

function App() {
  return (
    <div>
      <Routes />
    </div>
  );
}

export default App;
