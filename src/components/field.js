import React from 'react';

const Field = ({ type, name, placeholder, refs, error }) => {
    return (
        <div>
            <div className="ai-input">
                <input 
                    className={error && "ai-error"} 
                    type={type} name={name} 
                    placeholder={placeholder} 
                    ref={refs} 
                />
                <div className="ai-error-font">{error && error}</div>
            </div>
        </div>
    )
}

export { Field }