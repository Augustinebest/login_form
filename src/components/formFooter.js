import React from 'react';

const FormFooter = () => {
    return (
        <div className="ai-footer-links ai-text-align-center">
            <span className="ai-font-13">Don’t Have Access?</span> <span><a className="ai-a ai-font-13" href="#">Join the Waitlist</a></span><span> • &nbsp;&nbsp;&nbsp;<a className="ai-a" href="#">Contact Support</a></span>
        </div>
    )
}

export { FormFooter }