import React from 'react';
import './component.css';
import { Navbar } from './index';

const Layout = (props) => {
    return (
        <div className="ai-layout-bg">
            <Navbar />
            {props.children}
        </div>
    )
}

export { Layout };