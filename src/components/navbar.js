import React from 'react';
import logo from '../assets/logo.png';
import label from '../assets/othersideai.png';

const Navbar = () => {
    return (
        <div className="ai-navbar">
            <div className="ai-nav-links">
                <img className="ai-logo" src={logo} alt="" />
                <img className="ai-label" src={label} alt="" />
            </div>
        </div>
    )
}

export { Navbar }; 