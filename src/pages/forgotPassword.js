import React from 'react';
import { NavLink } from 'react-router-dom';
import { Field, FormFooter } from '../components';
import { useForm } from 'react-hook-form';
import { emailPattern } from '../utils/patterns'

const ForgotPassword = () => {
    const { register, handleSubmit, errors } = useForm();

    const onSubmit = (data) => {
        console.log(data)
    }

    return (
        <div className="ai-register-form">
            <div>
                <div className="ai-signup-label">Password Reset</div>
                <form className="ai-form" onSubmit={handleSubmit(onSubmit)}>
                <Field 
                        type="email"
                        name="email"
                        placeholder="Email"
                        refs={register({ 
                            required: "email is required",
                            validate: (value) => {
                                return (
                                    emailPattern.test(value) || "invalid email provided"
                                );
                            }
                         })}
                        error={errors.email && errors.email.message}
                    />
                    <div className="ai-input">
                        <button className="ai-input-btn" type="submit">Confirm</button>
                    </div>
                </form>
                <div className="ai-text-align-center ai-account">
                    <span className="ai-font-13"><span><NavLink to="/login" className="ai-signin ai-font-13">Back to Log In</NavLink></span></span>
                </div>
            </div>
            <FormFooter />
        </div>
    )
}

export { ForgotPassword };