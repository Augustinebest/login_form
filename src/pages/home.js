import React, { useEffect } from 'react';

const Home = () => {
    useEffect(() => {
        window.location.href = '/register'
    }, [])
    return (
        <div>
            Home
        </div>
    )
}

export { Home };