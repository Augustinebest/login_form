import React from 'react';
import { NavLink } from 'react-router-dom';
import google from '../assets/google.png';
import { Field, FormFooter } from '../components';
import { useForm } from 'react-hook-form';
import { emailPattern } from '../utils/patterns'

const Login = () => {
    const { register, handleSubmit, errors } = useForm();

    const onSubmit = (data) => {
        console.log(data)
    }

    return (
        <div className="ai-register-form">
            <div>
                <div className="ai-signup-label">Sign In</div>
                <form className="ai-form" onSubmit={handleSubmit(onSubmit)}>
                    <Field
                        type="email"
                        name="email"
                        placeholder="Email"
                        refs={register({
                            required: "email is required",
                            validate: (value) => {
                                return (
                                    emailPattern.test(value) || "invalid email provided"
                                );
                            }
                        })}
                        error={errors.email && errors.email.message}
                    />
                    <Field
                        type="password"
                        name="password"
                        placeholder="Password"
                        refs={register({
                            required: "password is required",
                            minLength: {
                                value: 8,
                                message: "password must be atleast 8 characters",
                            }
                        })}
                        error={errors.password && errors.password.message}
                    />
                    <div className="ai-input">
                        <button className="ai-input-btn" type="submit">Sign In</button>
                    </div>
                </form>
                <div className="ai-or-label">OR</div>
                <div className="ai-input">
                    <button className="ai-input-btn ai-google-signin" type="submit"><img src={google} alt="" /> <span>Continue with Google</span></button>
                </div>
                <div className="ai-text-align-center ai-account">
                    <span className="ai-font-13"><span><NavLink to="/register" className="ai-signin ai-font-13">Create Account</NavLink></span> &nbsp;&nbsp;&nbsp;<span>• &nbsp;&nbsp;&nbsp;<NavLink to="/forgotPassword" className="ai-signin ai-font-13">Forgot Password</NavLink></span></span>
                </div>
            </div>
            <FormFooter />
        </div>
    )
}

export { Login };