import React from 'react';
import google from '../assets/google.png';
import { NavLink } from 'react-router-dom';
import { Field, FormFooter } from '../components';
import { useForm } from 'react-hook-form';
import { emailPattern } from '../utils/patterns'


const Register = () => {
    const { register, handleSubmit, errors } = useForm();

    const onSubmit = (data) => {
        console.log(data)
    }
    
    return (
        <div className="ai-register-form">
            <div>
                <div className="ai-signup-label">Beta Sign Up</div>
                <form className="ai-form" onSubmit={handleSubmit(onSubmit)}>
                    <Field 
                        type="email"
                        name="email"
                        placeholder="Email"
                        refs={register({ 
                            required: "email is required",
                            validate: (value) => {
                                return (
                                    emailPattern.test(value) || "invalid email provided"
                                );
                            }
                         })}
                        error={errors.email && errors.email.message}
                    />
                    <Field 
                        type="password"
                        name="password"
                        placeholder="Password"
                        refs={register({
                            required: "password is required",
                            minLength: {
                              value: 8,
                              message: "password must be atleast 8 characters",
                            }
                          })}
                        error={errors.password && errors.password.message}
                    />
                    <Field 
                        type="text"
                        name="name"
                        placeholder="Name"
                        refs={register({ required: "name is required" })}
                        error={errors.name && errors.name.message}
                    />
                    <div className="ai-input">
                        <button className="ai-input-btn" type="submit">Sign Up</button>
                    </div>
                </form>
                <div className="ai-or-label">OR</div>
                <div className="ai-input">
                    <button className="ai-input-btn ai-google-signin" type="submit"><img src={google} alt="google" /> <span>Continue with Google</span></button>
                </div>
                <div className="ai-text-align-center ai-account">
                    <span className="ai-font-13">Already have an account? <NavLink to="/login" className="ai-signin ai-font-13">Sign In</NavLink></span>
                </div>
                <div className="ai-terms ai-text-align-center ai-font-13">
                    <p>
                        By signing up you agree to our <a className="ai-a ai-font-13 ai-font-light" href="!#">Terms of Service</a> and <a className="ai-a ai-font-13 ai-font-light" href="!#">Privacy Policy.</a>
                        You also agree to receive product-related marketing emails from OthersideAI, which you can unsubscribe from at any time.
                    </p>
                    <p>
                        This site is protected by reCAPTCHA and the Google <a className="ai-a ai-font-13 ai-font-light" href="!#">Privacy Policy</a> and <a className="ai-a ai-font-13 ai-font-light" href="!#">Terms of Service</a> apply.
                    </p>
                </div>
            </div>
            <FormFooter />
        </div>
    )
}

export { Register };