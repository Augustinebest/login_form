import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Layout } from './components';
import { Register, Login, ForgotPassword, Home } from './pages';

const Routes = (props) => {
    const AuthLayoutWrapper = props => {
        return (
            <Layout>
                <props.Component {...props} />
            </Layout>
        )
    }

    const AuthRoutes = ({ Component, path, exact, ...rest }) => {
        return (
            <Route 
                exact={exact || true}
                path={path}
                render={props => {
                    return <AuthLayoutWrapper {...props} {...rest} Component={Component} />
                }}
            />
        )
    }
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" component={Home} exact {...props} />
                <AuthRoutes path="/register" Component={Register} exact {...props} />
                <AuthRoutes path="/login" Component={Login} exact {...props} />
                <AuthRoutes path="/forgotPassword" Component={ForgotPassword} exact {...props} />
            </Switch>
        </BrowserRouter>
    )
}

export default Routes;